function Persona(nombre,apellido) {
  this.nombre = nombre;
  this.apellido = apellido;
  this.edad = 27;

  this.imprimirPersona = function () {
    return this.nombre + " " + this.apellido + "("+ this.edad +")";
  }
}

var sam = new Persona("Samuel","Arrieta");

console.log(sam.imprimirPersona());
