/*
for (var i = 0; i <= 10; i++){


  console.log(i);
}
*/

var Persona = function () {
  this.nombre = "Samuel",
  this.edad = 27,
  this.apellido = "Arrieta"
};

var samuel = new Persona();

Persona.prototype.direccion = "Leon"

for (prop in samuel) {

  if(samuel.hasOwnProperty(prop))
  continue;

  console.log(prop, ":", samuel[prop]);
}

for(num in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]){
  console.log(num);
}

[1, 2, 3, 4, true, 6, 7, 8, false, "nombre"].forEach(function (val) {
  console.log(val);
});
