// Tipos de variables

var booleano = true;
var string = "texto";
var entero = 5;
var flotante = 6.7;

// Arreglos

var papa = ["Francisco Javier Arrieta Padilla", "9 de Diciembre"];
var mama = ["Maria Elena Tavares Acosta", "11 de Septiembre"];
var hermana = ["Brenda Elena Arrieta Tavares", "23 de Agosto"];
var hermano = ["DIego Arrieta Tavares", "14 de Mayo"];
