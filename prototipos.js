function Persona() {
  this.nombre = "samuel";
  this.apellido = "Arrieta"
  this.edad = 30;
}


Persona.prototype.imprimirInfo = function () {
  console.log(this.nombre + " " + this.apellido + "("+ this.edad +")");
}

var sam = new Persona();

Number.prototype.esPositivo = function () {
  if (this > 0) {
    return true;
  } else {
    return false;
  }
}
