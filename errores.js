/*
try {
  var a = 100;

  if (a === 100) {
    // obligar a tirar error con "throw" y este no funciona fuera del try
    throw 'que mal';
  } else {
    throw 'oh oh!'
  }

  console.log("El valor de a: ", a);

} catch (e){

  if (e === "que mal") {
    console.log("Error tipo 1");
  }
  if (e === "oh oh!"){
    console.log("Error tipo 2");
  }

  console.log("Error de catch: ", e);

} finally {

  console.log("finalmente");

}

// el try debe llevar forzosamente minimo uno de los 2 operadores (catch o finally)
*/

/*

try {

  throw {
    nombreError: "Error tipo 1",
    accion: "Salir corriendo a hecharle agua al servidor",
    coderror: 1
  }

  console.log("Esta parte nunca se ejecuta");

} catch (e) {

  console.log(e);
  console.log(e.nombreError);
  console.log(e.accion);
  console.log(e.coderror);

  console.log("parte del catch");
} finally {

  console.log("finalmente");

}
*/
/*
try {

  throw function () {
    console.log("Funcion en el throw");
  }

  console.log("Esta parte nunca se ejecuta");

} catch (e) {

  e();

  console.log("parte del catch");
} finally {

  console.log("finalmente");

}
*/

try {

  throw 1;

  console.log("Esta parte nunca se ejecuta");

} catch (e) {

  registroError(e)

} finally {

  console.log("finalmente");

}

function registroError(e) {

  var ahora = new Date ();

  console.log("Se registro un error: ", e, " a las ", ahora);

}
