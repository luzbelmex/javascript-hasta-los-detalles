function Panzer () {
  this.nombre = "panzer";
  this.pv = 1500; // vida
  this.pn = 150; // penetracion
  this.bl = 120; // blindaje
  this.dn = 139; // daño

  this.ataque = function (tanqueObjetivo) {
    if (tanqueObjetivo.pv > 139) {
      tanqueObjetivo.pv -= 139;
    } else {
      tanqueObjetivo.pv = 0;
      console.error(tanqueObjetivo + " fue destruido");
    }
  }
// cambiar esta parte
  this.estado = function (tanqueObjetivo) {
    console.info(this);
    console.info(tanqueObjetivo);
  }
}

function Sherman() {
  this.nombre = "sherman";
  this.pv = 1350;
  this.pn = 128;
  this.bl = 101;
  this.dn = 115;

  this.ataque = function (tanqueObjetivo) {
    if (tanqueObjetivo.pv > 115) {
      tanqueObjetivo.pv -= 115;
    } else {
      tanqueObjetivo.pv = 0;
      console.error(tanqueObjetivo + " fue destruido");
    }
  }

  this.estado = function (tanqueObjetivo) {
    console.info(this);
    console.info(tanqueObjetivo);
  }
}

function Progeto() {
  this.nombre = "progeto";
  this.pv = 1300;
  this.pn = 165;
  this.bl = 100;
  this.dn = 240;

  this.ataque = function (tanqueObjetivo) {
    if (tanqueObjetivo.pv > 240) {
      tanqueObjetivo.pv -= 240;
    } else {
      tanqueObjetivo.pv = 0;
      console.error(tanqueObjetivo + " fue destruido");
    }
  }

  this.estado = function (tanqueObjetivo) {
    console.info(this);
    console.info(tanqueObjetivo);
  }
}

function Centurion() {
  this.nombre = "centurion";
  this.pv = 1350;
  this.pn = 148;
  this.bl = 124;
  this.dn = 140;

  this.ataque = function (tanqueObjetivo) {
    if (tanqueObjetivo.pv > 140) {
      tanqueObjetivo.pv -= 140;
    } else {
      tanqueObjetivo.pv = 0;
      console.error(tanqueObjetivo + " fue destruido");
    }
  }

  this.estado = function (tanqueObjetivo) {
    console.info(this);
    console.info(tanqueObjetivo);
  }
}

function Matilda () {
  this.nombre = "matilda";
  this.pv = 1200;
  this.pn = 126;
  this.bl = 120;
  this.dn = 160;

  this.ataque = function (tanqueObjetivo) {
    if (tanqueObjetivo.pv > 160) {
      tanqueObjetivo.pv -= 160;
    } else {
      tanqueObjetivo.pv = 0;
      console.error(tanqueObjetivo + " fue destruido");
    }
    this.estado = function (tanqueObjetivo) {
      console.info(this);
      console.info(tanqueObjetivo);
    }
  }

  this.estado = function (tanqueObjetivo) {
    console.info(this);
    console.info(tanqueObjetivo);
  }
}

var panzer = new Panzer;
var sherman = new Sherman;
var progeto = new Progeto;
var centurion = new Centurion;
var matilda = new Matilda;

console.log(panzer); // aleman
console.log(sherman); // estado unidence
console.log(progeto); // italiano
console.log(centurion); // ingles
console.log(matilda); // ruso
