var arr = [
  true,
  {
  nombre: "Samuel",
  apellido: "Arrieta"
},
  function () {
    console.log("Estoy viviendo en un Arrelo");
  },
  function (persona) {
    console.log(persona.nombre + " " + persona.apellido);
  },
  [
    "francisco",
    "samuel",
    "maria",
    "guadalupe",
    "panfilo",
    [
      "javier",
      "brenda",
      "diego",
      function () {
        console.log(this);
      }
    ]
]

];

console.log(arr);
console.log(arr[0]);
console.log(arr[1].nombre + " " + arr[1].apellido);

arr[2]();
arr[3](arr[1])

console.log(arr[4][5][1]);

var arreglo2 = arr[4][5];

console.log(arreglo2);

arreglo2 [1] = "elena";

console.log(arreglo2);
console.log(arr);

arreglo2[3]();
